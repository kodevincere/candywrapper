package com.kodevincere.candywrapper.lib;

import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by James
 */
public class CandyWrapper {

    private TextView textView;
    private StringBuilder textOfTextView;
    private List<Candy> listCandies = new ArrayList<>();
    private SpannableString spannable = null;

    public static CandyWrapper on(TextView tv){
        return new CandyWrapper()
                .textView(tv);
    }

    private CandyWrapper textView(TextView textView) {
        this.textView = textView;
        return text(textView.getText().toString());
    }

    private CandyWrapper text(String text) {
        this.textOfTextView = new StringBuilder(text);
        return this;
    }

    public CandyWrapper addCandy(Candy candy) {
        if (candy == null) {
            throw new IllegalArgumentException("wrapper is null");
        }
        this.listCandies.add(candy);
        return this;
    }

    public void wrap(){
        int size = listCandies.size(), i = -1;

        while(++i < size){
            Candy ca = listCandies.get(i);
            if(ca.getType() == Candy.TYPE_TEXT){

            }else{
                Pattern pattern = ca.getPattern();
                Matcher m = pattern.matcher(textOfTextView);



            }
        }

        turnPatternsToCandies();
        if(listCandies.isEmpty())
            return;

        for (Candy candy : listCandies) {
            addCandyToSpan(candy);
        }

        textView.setText(spannable);
    }

    private void turnPatternsToCandies() {
        int size = listCandies.size(), i = 0;
        while (i < size) {
            if (listCandies.get(i).getType() == Candy.TYPE_TEXT_PATTER) {
                addCandiesFromPattern(listCandies.get(i));

                listCandies.remove(i);
                size--;
            } else {
                i++;
            }
        }
    }

    private void addCandiesFromPattern(Candy candyWithPattern) {
        Pattern pattern = candyWithPattern.getPattern();
        Matcher m = pattern.matcher(textOfTextView);

        while (m.find()) {
            Candy c = Candy.convert(candyWithPattern, m.group());
            listCandies.add(c);
        }
    }

    private void addCandyToSpan(Candy candy) {

        Pattern pattern = Pattern.compile(Pattern.quote(candy.getSearchText()));
        Matcher matcher = pattern.matcher(textOfTextView);

        Log.d("JAMESJAMES", candy.toString());
        Log.d("JAMESJAMES", textOfTextView.toString());
        List<Range> listRange = new ArrayList<>();

        int lenSearch = candy.getSearchText().length();
        int lenReplace = candy.getReplaceText().length();

        int diff = Math.abs(lenReplace - lenSearch);
        int accumulated = diff;
        boolean who = lenReplace > lenSearch;

        int i = 0;
        while (matcher.find()) {
            int start = matcher.start();
            if (start >= 0) {
                int end = start + lenSearch;

                if(candy.isReplace()) {
                    if (i == 0) {
                        if (who)
                            end += diff;
                        else
                            end -= diff;
                    } else {
                        if (who) {
                            start += accumulated;
                            accumulated += diff;
                            end += accumulated;
                        } else {
                            start -= accumulated;
                            accumulated += diff;
                            end -= accumulated;
                        }
                    }
                }

                listRange.add(new Range(start, end));
            }

            i++;
        }











        if(candy.isReplace()) {
            matcher = pattern.matcher(textOfTextView);
            textOfTextView = new StringBuilder(matcher.replaceAll(candy.getReplaceText()));
        }

        Log.d("JAMESJAMES", textOfTextView.toString());


        if (spannable == null) {
            spannable = SpannableString.valueOf(textOfTextView);
        }

        for (Range r: listRange) {
            applyCandy(candy, r, spannable);
        }
    }

    private void applyCandy(Candy candy, Range range, Spannable spannable) {

        ForegroundColorSpan fc = new ForegroundColorSpan(candy.getTextColor());
        spannable.setSpan(fc, range.start, range.end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        BackgroundColorSpan bc = new BackgroundColorSpan(candy.getBackgroundColor());
        spannable.setSpan(bc, range.start, range.end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        RelativeSizeSpan rs = new RelativeSizeSpan(candy.getSize());
        spannable.setSpan(rs, range.start, range.end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        if(candy.isBold()){
            StyleSpan span = new StyleSpan(Typeface.BOLD);
            spannable.setSpan(span, range.start, range.end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        if(candy.isItalic()){
            StyleSpan span = new StyleSpan(Typeface.ITALIC);
            spannable.setSpan(span, range.start, range.end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        if(candy.isUnderlined()){
            UnderlineSpan span = new UnderlineSpan();
            spannable.setSpan(span, range.start, range.end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        if(candy.isStrikethrough()){
            StrikethroughSpan span = new StrikethroughSpan();
            spannable.setSpan(span, range.start, range.end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }
}

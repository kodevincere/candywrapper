package com.kodevincere.candywrapper.lib;

import android.graphics.Color;
import android.util.Log;

import java.util.regex.Pattern;

/**
 * Created by James
 */
public class Candy {

    static final int TYPE_TEXT = 1;
    static final int TYPE_TEXT_PATTER = 2;

    private String searchText;
    private Pattern pattern;
    private int type;

    private String replaceText;
    private boolean replace;

    private int textColor;
    private int backgroundColor;
    private float alpha;
    private float size;
    private boolean underlined;
    private boolean strikethrough;
    private boolean bold;
    private boolean italic;

    private Range range;


    public static Candy search(String search) {
        return new Candy(search, Pattern.compile(""), TYPE_TEXT);
    }

    public static Candy search(Pattern pattern) {
        return new Candy("", pattern, TYPE_TEXT_PATTER);
    }

    private Candy(String search, Pattern pattern, int type) {
        this.searchText = search;
        this.pattern = pattern;
        this.type = type;

        this.replaceText = "";
        this.textColor = Color.parseColor("#6d6e71");
        this.backgroundColor = Color.parseColor("#00000000");
        this.alpha = 1f;
        this.size = 1f;
        this.underlined = false;
        this.strikethrough = false;
        this.bold = false;
        this.italic = false;

    }

    private Candy(Candy candy, String text) {
        Log.d("JAMESJAMES", "SEARCH: "+ text);
        this.searchText = text;
        this.type = TYPE_TEXT;

        Log.d("JAMESJAMES", "REPLACE TEST: "+ candy.getReplaceText());
        this.replaceText = candy.getReplaceText();
        this.replace = candy.isReplace();
        this.textColor = candy.getTextColor();
        this.backgroundColor = candy.getBackgroundColor();
        this.alpha = candy.getAlpha();
        this.size = candy.getSize();
        this.underlined = candy.isUnderlined();
        this.strikethrough = candy.isStrikethrough();
        this.bold = candy.isBold();
        this.italic = candy.isItalic();
    }

    public Candy replace(String replace) {
        if (replace == null) this.replaceText = "";
        else {
            this.replaceText = replace;
            this.replace = true;
        }

        return this;
    }

    static Candy convert(Candy candy, String text) {
        return new Candy(candy, text);
    }

    public Candy textColor(int color) {
        this.textColor = color;
        return this;
    }

    public Candy backgroundColor(int bgColor) {
        this.backgroundColor = bgColor;
        return this;
    }

    public Candy alpha(float alpha) {
        this.alpha = alpha;
        return this;
    }

    public Candy bold() {
        this.bold = true;
        return this;
    }

    public Candy italic() {
        this.italic = true;
        return this;
    }

    public Candy underline() {
        this.underlined = true;
        return this;
    }

    public Candy strikethrough() {
        this.strikethrough = true;
        return this;
    }

    public Candy size(float s){
        this.size = s;
        return this;
    }

    public void setRange(Range range) {
        this.range = range;
    }

    public String getSearchText() {
        return searchText;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public int getType() {
        return type;
    }

    public String getReplaceText() {
        return replaceText;
    }

    public boolean isReplace() {
        return replace;
    }

    public int getTextColor() {
        return textColor;
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    public float getAlpha() {
        return alpha;
    }

    public float getSize() {
        return size;
    }

    public boolean isUnderlined() {
        return underlined;
    }

    public boolean isStrikethrough() {
        return strikethrough;
    }

    public boolean isBold() {
        return bold;
    }

    public boolean isItalic() {
        return italic;
    }

    @Override
    public String toString() {
        return "Candy{" +
                "searchText='" + searchText + '\'' +
                ", pattern=" + pattern +
                ", type=" + type +
                ", replaceText='" + replaceText + '\'' +
                ", replace=" + replace +
                ", textColor=" + textColor +
                ", backgroundColor=" + backgroundColor +
                ", alpha=" + alpha +
                ", size=" + size +
                ", underlined=" + underlined +
                ", strikethrough=" + strikethrough +
                ", bold=" + bold +
                ", italic=" + italic +
                '}';
    }
}

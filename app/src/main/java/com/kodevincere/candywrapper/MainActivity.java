package com.kodevincere.candywrapper;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.kodevincere.candywrapper.lib.Candy;
import com.kodevincere.candywrapper.lib.CandyWrapper;

import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    TextView tvOne, tvTwo;
    int colorBlack;
    int colorBlue;
    int colorGrey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvOne = (TextView) findViewById(R.id.tv_one);
        tvTwo = (TextView) findViewById(R.id.tv_two);

        colorBlack = getResources().getColor(R.color.colorBlack);
        colorBlue = getResources().getColor(R.color.colorBlue);
        colorGrey = getResources().getColor(R.color.colorGrey);

        makeCandies1();
        //makeCandies2();
    }

    private void makeCandies1(){
        Candy candy = Candy.search("{nombre}")
                .replace("James Cardona")
                .backgroundColor(colorBlue)
                .underline();

        Candy candy2 = Candy.search("{hattrick}")
                .replace("AMIGOS")
                .bold()
                .italic();

        CandyWrapper.on(tvOne)
                .addCandy(candy)
                .addCandy(candy2)
                .wrap();
    }

    private void makeCandies2(){
        Candy candy = Candy.search(Pattern.compile("@\\w{1,15}"))
                .bold()
                .underline();

        Candy candy2 = Candy.search(Pattern.compile("#\\w{1,15}"))
                .replace("hashtag")
                .italic()
                .size(1.3f);

        CandyWrapper.on(tvTwo)
                .addCandy(candy)
                .addCandy(candy2)
                .wrap();
    }
}
